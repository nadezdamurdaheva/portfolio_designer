import React from "react";
import "./AboutMe.css";

export const AboutMe = () => {
  return (
    <section className="aboutMe aboutMe__container">
        <p className="header text">About me</p>
        <p className="text">
          Hello, I'm Ildar – designer from Moscow.<br/>
          I'm interested in design and everything connected with it.
        </p>
        <p className="text">
          I have worked in companies such as LG, Samsung and have extensive experience in design.
        </p>
        <p className="text">
          Ready to implement excellent projects with wonderful people.
        </p>
    </section>
  );
};
